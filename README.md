Voici le site web réalisé par Alec Petit-Siejak et Gabin Crozet dans le cadre d'un travail évalué à l'IUT Informatique Lyon 1 par les enseignants de Web et de Communication.

Le sujet donné était : "Les différents modes de déplacements doux pour venir ou se déplacer à l’intérieur de l’université"
